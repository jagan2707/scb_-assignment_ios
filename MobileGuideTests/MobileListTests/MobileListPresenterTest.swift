//
//  MobileListPresenterTest.swift
//  MobileGuideTests
//
//  Created by Chartchai Pothong on 27/01/2019.
//  Copyright © 2019 11 infotech. All rights reserved.
//

import XCTest
@testable import MobileGuide
class MobileListPresenterTest: XCTestCase {

  var mobileListPresenter = MobileListPresenter()

    override func setUp() {
      super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

  class MobileListPresenterOutputSpy: MobileListDisplayLogic {
    var displayMobileListCalled = false
    var displayFailureAlertCalled = false
    var updatingSortingListCalled = false
    
    func displayMobileList(viewModel: MobileList.getMobileList.ViewModel) {
      displayMobileListCalled = true
    }
    
    func showAlert(alertString: MobileList.mobilListFailure.Responce) {
      displayFailureAlertCalled = true
    }
    
    func updateSortingList(viewModel: MobileList.SortedMobileList.ViewModel) {
      updatingSortingListCalled = true
    }
  }

  //MARK: Tests
  
  func testPresentDisplayMobileShouldAskViewControllerToDisplayMobileList() {
    //Given
    let mobileListPresenterOutputSpy = MobileListPresenterOutputSpy()
    mobileListPresenter.viewController = mobileListPresenterOutputSpy
    let response = MobileList.getMobileList.Response(results:[MobileListModel]())
    //When
    mobileListPresenter.presentMobileList(response: response)
    
    //Then
    XCTAssertTrue(mobileListPresenterOutputSpy.displayMobileListCalled)
  }
  
  func testPresentFailureAlertShouldAskViewControllerToDisplayErrorAlert() {
    //Given
    let mobileListPresenterOutputSpy = MobileListPresenterOutputSpy()
    mobileListPresenter.viewController = mobileListPresenterOutputSpy
    
    //When
    mobileListPresenter.showFailureAlert(alertString: MobileList.mobilListFailure.Responce(alertString: "Something went wrong"))
    
    //Then
    XCTAssertTrue(mobileListPresenterOutputSpy.displayFailureAlertCalled)
  }
  
  func testPresentSortResultShouldAskViewControllerToDisplaySortedResult() {
    //Given
    let mobileListPresenterOutputSpy = MobileListPresenterOutputSpy()
    mobileListPresenter.viewController = mobileListPresenterOutputSpy
    let response = MobileList.SortedMobileList.Response(results:[MobileListModel]())
    
    //When
    mobileListPresenter.updateSortedList(response: response)
    
    //Then
    XCTAssertTrue(mobileListPresenterOutputSpy.updatingSortingListCalled)
  }
  
}
