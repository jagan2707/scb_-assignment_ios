//
//  MobileGuideTests.swift
//  MobileGuideTests
//
//  Created by Chartchai Pothong on 25/01/2019.
//  Copyright © 2019 11 infotech. All rights reserved.
//

import XCTest
@testable import MobileGuide

class MobileListInteractorTest: XCTestCase {
  
  var mobileListInteractor = MobileListInteractor()

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
       super.tearDown()
    }
  class MobileListWorkerSpy: MobileListWorker {
    
    var isGetMobiles = false
    var isFailure = false
    override  func getMobileList(request: MobileList.getMobileList.Request, onSuccess success: @escaping (_ result: [MobileListModel]) -> Void, onFailure failure: @escaping (_ error: Error?) -> Void) {
    isGetMobiles = true
    let mobileListEntity = MobileListModel(from: nil)
    
    if !isFailure {
      success([mobileListEntity])
    }
    else {
      failure(ReturnError.invalidJson)
    }
    }
  }
  
  class MobileListInteractorOutputSpy: MobileListPresentationLogic {
    var presentMobileList = false
    var showFailure = false
    var updateSortedList = false
    
    func presentMobileList(response: MobileList.getMobileList.Response) {
      presentMobileList = true
    }
    
    func showFailureAlert(alertString: MobileList.mobilListFailure.Responce) {
      showFailure = true
    }
    
    func updateSortedList(response: MobileList.SortedMobileList.Response) {
      updateSortedList = true
    }
  }
  
  //Test Cases
  func testGetMobileListSuccessShouldAskPresenterToDisplayList() {
    //Given
    let mobileListOutSpy = MobileListInteractorOutputSpy()
    let mobileListWorkerSpy = MobileListWorkerSpy()
    mobileListWorkerSpy.isFailure = false
    mobileListInteractor.presenter = mobileListOutSpy
    mobileListInteractor.worker = mobileListWorkerSpy
    let request = MobileList.getMobileList.Request()
    
    //When
    mobileListInteractor.getMobileList(request: request)

    //Then
    XCTAssertTrue(mobileListWorkerSpy.isGetMobiles)
    XCTAssertTrue(mobileListOutSpy.presentMobileList)
  }
  
  func testGetMobileListFailureShouldAskPresenterToDisplayList() {
    
    //Given
    let mobileListOutSpy = MobileListInteractorOutputSpy()
    let mobileListWorkerSpy = MobileListWorkerSpy()
    mobileListWorkerSpy.isFailure = true
    mobileListInteractor.presenter = mobileListOutSpy
    mobileListInteractor.worker = mobileListWorkerSpy
    let request = MobileList.getMobileList.Request()
    
    //When
    mobileListInteractor.getMobileList(request: request)
    
    //Then
    XCTAssertTrue(mobileListWorkerSpy.isGetMobiles)
    XCTAssertFalse(mobileListOutSpy.presentMobileList)
  }
  
  func testApplySortListShouldAskPresenterToDisplayList() {
    //Given
    let mobileListOutSpy = MobileListInteractorOutputSpy()
    mobileListInteractor.presenter = mobileListOutSpy
    
    //When
    mobileListInteractor.applySorting(request: MobileList.SortedMobileList.Request(sortMethod: .Price_High_To_Low))
    
    //Then
    XCTAssertTrue(mobileListOutSpy.updateSortedList)
  }
}
