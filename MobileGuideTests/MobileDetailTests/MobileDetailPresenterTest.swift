//
//  MobileDetailPresenterTest.swift
//  MobileGuideTests
//
//  Created by Chartchai Pothong on 27/01/2019.
//  Copyright © 2019 11 infotech. All rights reserved.
//

import XCTest
@testable import MobileGuide

class MobileDetailPresenterTest: XCTestCase {

  var mobileDetailsPresenter = MobileDetailsPresenter()

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
       super.tearDown()
    }

  class MobileDetailsPresenterOutPutSpy: MobileDetailsDisplayLogic {
    
    var displayMobileImageCalled = false
    var displayFailureAlertCalled = false
    
    func showMobileImages(viewModel: MobileDetails.mobileImages.ViewModel) {
      displayMobileImageCalled = true
    }
    
    func showMobileImagesDownloadFailureAlert(viewModel: MobileDetails.mobileImages.mobileImagesfailure) {
      displayFailureAlertCalled = true
    }
  }

  //MARK: Tests
  
  func testPresentMobileImagesShouldAskViewControllerToDisplayMobileImages() {
    //Given
    let mobileDetailPresenterOutputSpy = MobileDetailsPresenterOutPutSpy()
    mobileDetailsPresenter.viewController = mobileDetailPresenterOutputSpy
    let response = MobileDetails.mobileImages.Response(result:[MobileImagesData]())
    
    //When
    mobileDetailsPresenter.presentMobileImageDetails(response: response)
    
    //Then
    XCTAssertTrue(mobileDetailPresenterOutputSpy.displayMobileImageCalled)
  }
  
  func testPresentFailureAlertShouldAskViewControllerToDisplayErrorAlert() {
    //Given
    let mobileDetailPresenterOutputSpy = MobileDetailsPresenterOutPutSpy()
    mobileDetailsPresenter.viewController = mobileDetailPresenterOutputSpy
    let response = MobileDetails.mobileImages.mobileImagesfailure(failure:"")
    
    //When
    mobileDetailsPresenter.presentMobileImageDetailsFailure(response: response)
    
    //Then
    XCTAssertTrue(mobileDetailPresenterOutputSpy.displayFailureAlertCalled)
  }
  
  
}
