//
//  MobileDetailInteractorTest.swift
//  MobileGuideTests
//
//  Created by Chartchai Pothong on 27/01/2019.
//  Copyright © 2019 11 infotech. All rights reserved.
//

import XCTest
@testable import MobileGuide

class MobileDetailInteractorTest: XCTestCase {

  var mobileDetailsInteractor = MobileDetailsInteractor()
  
  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }
 
  class MobileDetailWorkerSpy: MobileDetailsWorker {
    
    var isGetMobileDetails = false
    var isFailure = false
    override  func getMobileImageDetails(request:MobileDetails.mobileImages
      .Request,onSuccess success: @escaping (_ result: [MobileImagesData]) -> Void, onFailure failure: @escaping (_ error: Error?) -> Void) {
      
      isGetMobileDetails = true
      if !isFailure {
        success([MobileImagesData]())
      }
      else {
        failure(ReturnError.invalidJson)
      }
    }
    
  }

  class MobileDetailsInteractorOutPutSpy: MobileDetailsPresentationLogic {
    
    var presentMobileDetails = false
    var showFailure = false
    
    func presentMobileImageDetails(response:MobileDetails.mobileImages.Response) {
      presentMobileDetails = true
    }
    
    func presentMobileImageDetailsFailure(response:MobileDetails.mobileImages.mobileImagesfailure) {
       showFailure = true
    }
   }
  
  //MARK: Tests
  //Test Cases
  func testGetMobileImageDetailsSuccessShouldAskPresenterToDisplayList() {
    //Given
    let mobileListOutSpy = MobileDetailsInteractorOutPutSpy()
    let mobileDetailWorkerSpy = MobileDetailWorkerSpy()
    mobileDetailWorkerSpy.isFailure = false
    mobileDetailsInteractor.presenter = mobileListOutSpy
    mobileDetailsInteractor.worker = mobileDetailWorkerSpy
    let request = MobileDetails.mobileImages.Request()
    
    //When
    mobileDetailsInteractor.getMobileImageDetails(request: request)
    
    //Then
    XCTAssertTrue(mobileDetailWorkerSpy.isGetMobileDetails)
    XCTAssertTrue(mobileListOutSpy.presentMobileDetails)
  }
  
  func testGetMobileImageDetailsFailureShouldAskPresenterToShowFailureAlert() {
    //Given
    let mobileListOutSpy = MobileDetailsInteractorOutPutSpy()
    let mobileDetailWorkerSpy = MobileDetailWorkerSpy()
    mobileDetailWorkerSpy.isFailure = true
    mobileDetailsInteractor.presenter = mobileListOutSpy
    mobileDetailsInteractor.worker = mobileDetailWorkerSpy
    let request = MobileDetails.mobileImages.Request()
    
    //When
    mobileDetailsInteractor.getMobileImageDetails(request: request)
    
    //Then
    XCTAssertTrue(mobileDetailWorkerSpy.isGetMobileDetails)
    XCTAssertTrue(mobileListOutSpy.showFailure)
  }
}
