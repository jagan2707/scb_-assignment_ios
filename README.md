#Take Home assignment bySCB -Mobile Guide App Assignment

I have used Clean Swift architecture.

SOFTWARE TOOLS AND VERSIONS
----------------------------------------
* Mac OS 10.13.6
* Xcode 10.1

SUPPORT IOS VERSION 
----------------------------------------
* From 10.0+

Libraries I have used for the assignment
----------------------------------------
* SDWebImages
* Reachability


Developed By
------------

* Jagadeesh Naidu Kadumula
* Email: <jagadeesh.iosdeveloper@gmail.com>

