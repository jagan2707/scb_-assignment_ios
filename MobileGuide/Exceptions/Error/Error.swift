//
//  Error.swift
//  MobileGuide
//
//  Created by Chartchai Pothong on 26/01/2019.
//  Copyright © 2019 11 infotech. All rights reserved.
//

import Foundation

public enum MobileListResult<T>
{
  case success(result: T)
  case failure(error: Error)
}

public enum ReturnError: Error {
  case apiError(errorCode: String, headerText: String?, errorMessage: String?)
  case invalidJson
}

public enum Content<T> {
  case empty
  case error
  case success(data: T)
  case jsonError(error: Error)
}
