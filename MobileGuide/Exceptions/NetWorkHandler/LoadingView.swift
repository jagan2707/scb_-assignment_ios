//
//  LoadingView.swift
//  MobileGuide
//
//  Created by Chartchai Pothong on 27/01/2019.
//  Copyright © 2019 11 infotech. All rights reserved.
//

import UIKit

class LoadingView: UIView {
  
  private let spinner = UIActivityIndicatorView(style:.gray)
  
  override init(frame: CGRect) {
    
    spinner.startAnimating()
    
    super.init(frame: frame)
    
    addSubview(spinner)
    backgroundColor = UIColor.white
    self.alpha = 0.3
  }
  
  @available(*, unavailable)
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    spinner.center = center
  }
  
}
