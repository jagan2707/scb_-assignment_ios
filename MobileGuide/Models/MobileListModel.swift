//
//  MobileListModel.swift
//  MobileGuide
//
//  Created by Chartchai Pothong on 25/01/2019.
//  Copyright © 2019 11 infotech. All rights reserved.
//

import UIKit

public struct MobileListModel:Codable{
  public let id: Int!
  public let name: String?
  public let brand: String?
  public let description: String?
  public let thumbImage: String?
  public let price: Double?
  public let rating: Double?
  public var isFavourite: Bool?
  public init(id: Int!, name: String?, brand: String?, description: String?, thumbImage:String?, price: Double?, rating: Double?,favourite: Bool)
  {
    self.id = id
    self.name = name
    self.brand = brand
    self.description = description
    self.thumbImage = thumbImage
    self.price = price
    self.rating = rating
    self.isFavourite = favourite
  }
  
}

extension MobileListModel
{
  public init(from jsonData: [String: Any]?) {

    id = jsonData?["id"] as? Int
    name = jsonData?["name"] as? String ?? ""
    brand = jsonData?["brand"] as? String ?? ""
    description = jsonData?["description"] as? String ?? ""
    thumbImage = jsonData?["thumbImageURL"] as? String ?? ""
    price = jsonData?["price"] as? Double ?? 0
    rating = jsonData?["rating"] as? Double ?? 0
    isFavourite = false
  }
  
}
