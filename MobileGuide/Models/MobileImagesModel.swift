//
//  MobileListModel.swift
//  MobileGuide
//
//  Created by Chartchai Pothong on 25/01/2019.
//  Copyright © 2019 11 infotech. All rights reserved.
//

import UIKit

public struct MobileImagesData {
  public let id: Int
  public let mobileId: Int
  public let mobileImage: String?
  
  public init(id: Int!, mobileId: Int!, mobileImage: String?)
  {
    self.id = id
    self.mobileId = mobileId
    self.mobileImage = mobileImage
  }
  
}

extension MobileImagesData
{
  public init(from jsonData: [String: Any]?) {
    
    id = jsonData?["id"] as! Int
    mobileId = jsonData?["mobile_id"] as! Int
    mobileImage = jsonData?["url"] as? String ?? ""
    
  }
  
}
