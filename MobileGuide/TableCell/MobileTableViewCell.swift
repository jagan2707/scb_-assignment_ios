//
//  MobileTableView.swift
//  MobileGuide
//
//  Created by Chartchai Pothong on 25/01/2019.
//  Copyright © 2019 11 infotech. All rights reserved.
//

import UIKit
import SDWebImage

class MobileTableViewCell: UITableViewCell {

  @IBOutlet weak var moibleImageView: UIImageView!
  @IBOutlet weak var mobileTitle: UILabel!
  @IBOutlet weak var mobileDescription: UILabel!
  @IBOutlet weak var mobilePrice: UILabel!
  @IBOutlet weak var mobileRatings: UILabel!
  @IBOutlet weak var favouriteButton: UIButton!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

  class func getCellIdentifier() -> String
  {
    return "MobileCell"
  }
  
  func setUp(indexPath: IndexPath, mobile : MobileListModel) {
    favouriteButton.setImage(UIImage(named: "star"), for: .normal)
    favouriteButton.setImage(UIImage(named: "starSelected"), for: .selected)
    self.mobileTitle.text = mobile.name
    self.mobileDescription.text = mobile.description
    if let price = mobile.price,
      let rating = mobile.rating {
      self.mobilePrice.text = "Price: \(String(price))"
      self.mobileRatings.text = "Rating: \(String(rating))"
    }
    self.moibleImageView.sd_setImage(with: URL(string: mobile.thumbImage ?? ""), placeholderImage: UIImage(named: "placeholder"))
  }
}
